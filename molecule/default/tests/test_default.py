import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_keepalived_config_file(host):
    f = host.file('/etc/keepalived/keepalived.conf')
    assert f.exists


def test_keepalived_running(host):
    keepalived_svc = host.service("keepalived")
    keepalived_svc.is_running
